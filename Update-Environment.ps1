﻿param (
    [string] $NewEnvironment
)

$websites = Get-Website | Where {$_.name -like "*.master"}

ForEach($website in $websites) {
    $newName = $website.name.Replace(".master", $NewEnvironment)
    $oldName = $website.name

    Write-Host "Renaming $oldName -> $newName"

    Rename-Item "IIS:\Sites\$($website.name)" $newName

    $binding = Get-WebBinding -Name $newName
    
    Remove-WebBinding -HostHeader $website.name
    New-WebBinding -Name $newName -port 80 -HostHeader $newName
}