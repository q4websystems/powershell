param(
    [string] $Template,
    [string] $NewVMName   
)

$TemplateName = $Template
$NewName = $NewVMName
$TemplateDir = 'M:\VMTemplate'
$VMLocation = "M:\Hyper-V\VMs\$NewName"

# Make sure we aren't trying to create a VM that already exists.
$ExistingVM = Get-VM -Name $NewName
if($ExistingVM.Name -eq $NewName) {
    throw "The Virtual Machine you are trying to create '$NewName' already exists, please choose a new name."
}

$ImportFile = Get-ChildItem -Path "$TemplateDir\$TemplateName\Virtual Machines\"

Write-Host "Importing"
$ImportJob = Import-VM -Path "$TemplateDir\$VMName\Virtual Machines\$ImportFile" -VhdDestinationPath $VMLocation -VirtualMachinePath $VMLocation -SnapshotFilePath $VMLocation -Copy -GenerateNewId
Write-Output "Finished Importing"

Write-Host "Renaming to $NewName"
Foreach ($Import in $ImportJob)
{
    $oldName = $Import.Name
    Rename-VM $oldName �NewName $NewName
}


# Get the network adapter for reference
#$NetworkAdapter = Get-VMNetworkAdapter -VMName $NewName

# Drop network card
#Remove-VMNetworkAdapter -VMName $NewName

# Recreate network card
#Add-VMNetworkAdapter -VMName $NewName -SwitchName "Intel(R) I350 Gigabit Network Connection #2 - Virtual Switch"

Start-VM -Name $NewName
# Remove-Item -Recurse -Force "$ExportDir\$VMName"