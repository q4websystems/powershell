﻿# You can run this against remote machines like so.
# Invoke-Command -ComputerName Appworker-1,Appworker-2 -Credential q4websystems\q4setupuser -FilePath D:\Project\Q4Orion\Powershell\dev\Create-Worker-Queues.ps1 -ArgumentList sqx

Param(
	[Parameter(Mandatory=$True)]
	[String] $prefix
)

Add-Type -AssemblyName System.Messaging

Function GetGroupName
{
    param(
        [Parameter(Mandatory=$true)]
        [System.Security.Principal.WellKnownSidType] $wellKnownSidType
    )

    $account = New-Object System.Security.Principal.SecurityIdentifier($wellKnownSidType, $null)
    return $account.Translate([System.Security.Principal.NTAccount]).ToString()
}

Function AssignPermissions($queue) 
{
    $localName = $env:COMPUTERNAME
    $queue.SetPermissions("dev.office.q4inc.com\Domain Admins", "FullControl", "Allow")
    $queue.SetPermissions("dev.office.q4inc.com\ServiceBusUser", "FullControl", "Allow")
    $queue.SetPermissions($localName + "\Administrator", "FullControl", "Allow")
    $queue.SetPermissions($AnonymousLogin, "WriteMessage", "Allow")
    $queue.SetPermissions($EveryoneGroup, "WriteMessage", "Allow")

    Write-Host "Set permissions on" $queueName -ForegroundColor Green
    Write-Host ""
}

Function CreateQueue($queueName) 
{
    Write-Host Creating queue $queueName   
    if ([System.Messaging.MessageQueue]::Exists(".\private$\" + $queueName))
    {
      Write-Host "Queue" $queueName "exists, assigning permissions" -ForegroundColor Green
      $qb = New-Object System.Messaging.MessageQueue(".\private$\" + $queueName)
      AssignPermissions $qb
      return
    }

    $qb = [System.Messaging.MessageQueue]::Create(".\private$\" + $queueName, 1)
    Write-Host "Create new" $queueName -ForegroundColor Green

    AssignPermissions $qb
}

Write-Host ""
Write-Host "Creating msmq queues on worker with prefix $prefix on $env:COMPUTERNAME" -ForegroundColor Green
Write-Host ""

$EveryoneGroup=GetGroupName("WorldSid")
$AnonymousLogin=GetGroupName("AnonymousSid")

CreateQueue "d.$prefix.inq"
CreateQueue "d.$prefix.inq.distributor.control"
CreateQueue "d.$prefix.inq.distributor.storage"